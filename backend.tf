terraform {
  backend "s3" {
    bucket = "terraformklass"
    key    = "klass"
    region = "us-east-1"
  }
}
